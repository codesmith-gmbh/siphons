(ns regenerate
  (:require [babashka.fs :as fs]
            [clojure.edn :as edn]
            [clojure.string :as str]
            [clojure.pprint :as pp]
            [libs]))

(def project-dir (fs/path "."))

(defn deps-file [dir]
  (fs/file (fs/path dir "deps.edn")))

(defn slurp-deps [dir]
  (edn/read-string (slurp (deps-file dir))))

(defn block-dep? [[dep]]
  (and (= "ch.codesmith" (namespace dep))
       (str/starts-with? (name dep) "blocks")))

(defn merge-deps [acc dir]
  (let [pathify #(str (fs/relativize project-dir (fs/path dir %)))
        {:keys [paths deps aliases]} (slurp-deps dir)
        {:keys [extra-paths extra-deps]} (:test aliases)]
    (-> acc
        (update-in [:libs-dev :extra-paths] #(into % (map pathify) paths))
        (update-in [:libs-dev :extra-deps] #(into % (remove block-dep?) deps))
        (update-in [:test :extra-paths] #(into % (map pathify) extra-paths))
        (update-in [:test :extra-deps] #(into % (remove block-dep?) extra-deps)))))

(defn regenerate-dev-deps []
  (let [deps      (slurp-deps project-dir)
        libs-deps (reduce merge-deps
                          {:libs-dev {:extra-paths []
                                      :extra-deps  {}}
                           :test     {:extra-paths []
                                      :extra-deps  {}}}
                          libs/lib-dirs)
        new-deps  (update deps :aliases #(merge % libs-deps))]
    (spit (deps-file project-dir)
          (with-out-str
            (pp/pprint new-deps)))))
